<?php

namespace Components;

class Param
{
    public static function load($key)
    {
        $param = array_merge(
            require ROOT . '/config/params.php',
            require ROOT . '/config/params_local.php'
        );

        if(array_key_exists($key, $param)) {
            return $param[$key];
        } else {
            return '';
        }
    }
}
