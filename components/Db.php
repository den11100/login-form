<?php

namespace Components;

use PDO;

/**
 * Class Db
 */
class Db
{
    /**
     * @return \PDO
     */
    public static function getConnection()
    {
        $params_db = array_merge(
            require ROOT . '/config/db_params.php',
            require ROOT . '/config/db_params_local.php'
        );

        $dsn = "mysql:host={$params_db['db1']['host']};dbname={$params_db['db1']['dbname']}";
        $db = new PDO($dsn, $params_db['db1']['user'], $params_db['db1']['password']);

        $db->exec("set names utf8");

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
    }
}
