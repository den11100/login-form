<?php

namespace Controllers;

use Models\File;
use Models\Helper;
use Models\User;

class SiteController
{
    public function actionIndex()
    {
        if (isset($_POST['login'])) {

            $email = Helper::cleanContent($_POST['email']);
            $password = Helper::cleanContent($_POST['password']);

            $errors = [];
            if (empty($email)) $errors['email'] = _('Enter an email');
            if (!User::checkEmail($email)) $errors['email'] = _('Enter a valid email');

            $userId = User::login($email, $password);

            if (!$userId) {
                $errors['password'] = _('Incorrect login or password');
                $errors['email'] = _('Incorrect login or password');
            }

            $emailValidation = (isset($errors['email']) ? "is-invalid" : "is-valid");
            $passwordValidation = (isset($errors['password']) ? "is-invalid" : "is-valid");

            if ($errors == [] && $userId) {
                User::auth($userId);
                header("Location: /profile/view/" .$userId);
            }
        }

        $action = 'site\index';
        require_once(ROOT . '/views/site/index.php');        
        return true;
    }

    public function actionSignup()
    {
        if (!User::isGuest()) header("Location: /profile/view/" . $_SESSION['user']);

        if (isset($_POST['signup'])) {

            $email = Helper::cleanContent($_POST['email']);
            $password = Helper::cleanContent($_POST['password']);
            $repeatPassword = Helper::cleanContent($_POST['repeat-password']);
            $pathFiles = Helper::cleanContent($_POST['path-files']);

            $errors = [];
            if (empty($email)) $errors['email'] = _('Enter an email');
            if (!User::checkEmail($email)) $errors['email'] = _('Enter a valid email');
            if (User::checkEmailExists($email)) $errors['email'] = _('That email in system. Try another.');

            if (empty($password)) $errors['password'] = _('Enter a password');
            if (!User::checkPassword($password)) $errors['password'] = _('Use 6 characters or more for your password');

            if (empty($repeatPassword)) $errors['repeat-password'] = _('Confirm a password');
            if ($password !== $repeatPassword) $errors['repeat-password'] = _("Those passwords didn't match. Try again.");
            if (!User::checkPassword($repeatPassword)) $errors['repeat-password'] = _('Use 6 characters or more for your password');

            $emailValidation = (isset($errors['email']) ? "is-invalid" : "is-valid");
            $passwordValidation = (isset($errors['password']) ? "is-invalid" : "is-valid");
            $passwordRepeatValidation = (isset($errors['repeat-password']) ? "is-invalid" : "is-valid");

            if ($errors == []) {
                $userId = User::register($email, $password);
                User::auth($userId);

                if ($pathFiles) {
                    $pathFiles = explode(',', $pathFiles);
                    File::savePathFiles($pathFiles, $userId);
                }

                header("Location: /profile/view/" .$userId);
            }
        }

        $action = 'site\signup';
        require_once(ROOT . '/views/site/signup.php');
        return true;
    }

    public function actionLogout()
    {
        unset($_SESSION["user"]);
        header("Location: /");
        return true;
    }

    public function actionLanguage($lang)
    {
        if ($_SESSION['lang'] != $lang) {
            if ($lang == 'ru') $_SESSION['lang'] = 'ru';
            if ($lang == 'en') $_SESSION['lang'] = 'en';
            Helper::redirect();
        }
        return true;
    }
}
