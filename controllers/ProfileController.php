<?php

namespace Controllers;

use Models\File;
use Models\User;

class ProfileController
{
    public function actionView($id)
    {
        $userId = User::checkLogged();

        if ($userId !== $id) die("Доступ запрещён"); // only for test task

        $user = User::getUser($id);
        $files = File::getFilesByUser($id);

        require_once(ROOT . '/views/profile/view.php');
        return true;
    }
}
