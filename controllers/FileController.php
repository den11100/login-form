<?php

namespace Controllers;

use RuntimeException;

class FileController
{
    public function actionUpload()
    {
        header('Content-type:application/json;charset=utf-8');

        try {
            if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }

            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            $fileType = $_FILES['file']['type']; //returns the mimetype
            $allowed = array("image/jpeg", "image/gif", "image/png", "image/jpg");
            if(!in_array($fileType, $allowed)) throw new RuntimeException('forbidden type files');

            $fileSize = $_FILES['file']['size'];
            if ($fileSize > 1024 * 1024){
                throw new RuntimeException('max file size 1Mb');
            }

            $extension = explode('.',$_FILES["file"]["name"]);
            $path = sprintf('/uploads/%s_%s', uniqid(), date("d_m_Y")).'.'.array_pop($extension);

            if (!move_uploaded_file( $_FILES['file']['tmp_name'], ROOT.$path)) {
                throw new RuntimeException('Failed to move uploaded file.');
            }

            // All good, send the response
            echo json_encode([
                'status' => 'ok',
                'path' => $path
            ]);
        } catch (RuntimeException $e) {
            // Something went wrong, send the err message as JSON
            http_response_code(400);

            echo json_encode([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        return true;
    }
}