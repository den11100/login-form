<?php

namespace Models;

use Components\Db;
use PDO;

class User
{
    /**
     * @param integer $userId
     */
    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    /**
     * @return boolean
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    /**
     * @return mixed
     */
    public static function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        header("Location: /site/login");
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function checkEmailExists($email)
    {
        $db = Db::getConnection();

        $sql = 'SELECT COUNT(id) FROM user WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    /**
     * @param string $password
     * @return boolean
     */
    public static function checkPassword($password)
    {
        if (mb_strlen($password, 'UTF-8') >= 6) {
            return true;
        }
        return false;
    }

    /**
     * @param string $email
     * @param string $password
     * @return int|string
     */
    public static function register($email, $password)
    {
        $db = Db::getConnection();

        $password = password_hash($password, PASSWORD_DEFAULT);
        $sql = 'INSERT INTO user (email, password) VALUES (:email, :password)';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    /**
     * @param string $email
     * @param string $password
     * @return bool
     */
    public static function login($email, $password)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        $user = $result->fetch(PDO::FETCH_ASSOC);

        if ($user && password_verify($password, $user['password'])){
            return $user['id'];
        }
        return false;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getUser($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT id, email FROM user WHERE id = :id ';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();

        return $result->fetch(PDO::FETCH_ASSOC);
    }
}
