<?php


namespace Models;

use Components\Db;
use PDO;

class File
{
    /**
     * @param array $paths
     * @param int $userId
     */
    public static function savePathFiles($paths, $userId)
    {
        $db = Db::getConnection();

        // Start Transaction
        $db->beginTransaction();

        // Insert each record
        foreach ($paths as $path) {
            $result = $db->prepare('INSERT INTO file (`user_id`, `name`) VALUES (:user_id, :file_path)');
            $result->bindParam(':user_id', $userId, PDO::PARAM_INT);
            $result->bindParam(':file_path', $path, PDO::PARAM_STR);
            $result->execute();
            $result = null;
        }

        $db->commit();
    }

    public static function getFilesByUser($userId)
    {
        $db = Db::getConnection();

        $sql = 'SELECT `name` FROM file WHERE user_id = :user_id ';
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $userId, PDO::PARAM_INT);
        $result->execute();

        return $result->fetchAll(PDO::FETCH_COLUMN, 'name');
    }
}