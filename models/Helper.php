<?php

namespace Models;

class Helper
{
    public static function cleanContent($value = "")
    {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return $value;
    }

    public static function redirect()
    {
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            header("Location: {$_SERVER['HTTP_REFERER']}");
        } else {
            header("Location: /");
        }
    }
}
