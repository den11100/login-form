<?php

return [
    'file/upload' => 'file/upload',

    'profile/view/([0-9]+)' => 'profile/view/$1',

    'language/(.*)' => 'site/language/$1',

    'signup' => 'site/signup',

    'logout' => 'site/logout',

    'index.php' => 'site/index',



    '' => 'site/index',
];
