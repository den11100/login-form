<?php

/* @var array $errors */
/* @var string $email */
/* @var string $password */

?>

<?php include ROOT . '/views/layouts/header.php'; ?>
<div class="container-fluid">
    <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
        <div class="col-md-8 col-lg-6">
            <div class="login d-flex align-items-center py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-lg-8 mx-auto">
                            <h3 class="login-heading mb-4"><?= _('Welcome') ?></h3>
                            <form method="POST" action="/" autocomplete="off">
                                <div class="form-label-group">
                                    <input autocomplete="off" name="email" type="email" id="inputEmail" class="form-control <?php echo isset($emailValidation)?$emailValidation:''; ?>" placeholder="Email" required autofocus value="<?php echo isset($email)? $email:''; ?>">
                                    <label for="inputEmail">Email</label>
                                    <?php if (isset($errors['email'])): ?>
                                        <div class="invalid-feedback"><?php echo $errors['email']?></div>
                                    <?php endif; ?>
                                </div>

                                <div class="form-label-group">
                                    <input autocomplete="off" name="password" type="password" id="inputPassword" class="form-control <?php echo isset($passwordValidation)?$passwordValidation:''; ?>" placeholder="Пароль" required value="<?= isset($password)? $password:'';?>">
                                    <label for="inputPassword"><?= _('Password') ?></label>
                                    <?php if (isset($errors['password'])): ?>
                                        <div class="invalid-feedback"><?php echo $errors['password']?></div>
                                    <?php endif; ?>
                                </div>

                                <button name="login" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit"><?= _('Login') ?></button>
                                <a href="/signup" class="btn btn-lg btn-danger btn-block btn-signup text-uppercase font-weight-bold mb-2"><?= _('Sign Up') ?></a>
                                <div class="text-center">
                                    <a class="small" href="#"><?= _('Forgot password ?') ?></a></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>