<?php

/*
 * $var array $errors
 * $var string $email
 * $var string $password
 */

?>

<?php include ROOT. '/views/layouts/header.php'; ?>

<div class="container-fluid">
    <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
        <div class="col-md-8 col-lg-6">
            <div class="login d-flex align-items-center py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-lg-8 mx-auto">
                            <h3 class="login-heading mb-4"><?= _('Sign Up') ?></h3>
                            <form method="POST" autocomplete="off">
                                <div class="form-label-group">
                                    <input autocomplete="off" name="email" type="email" id="inputEmail" class="form-control <?php echo isset($emailValidation)?$emailValidation:''; ?>" placeholder="Email" required autofocus value="<?php echo isset($email)? $email:'';?>">
                                    <label for="inputEmail">Email</label>
                                    <?php if (isset($errors['email'])): ?>
                                        <div class="invalid-feedback"><?php echo $errors['email']?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="form-label-group">
                                    <input autocomplete="off" name="password" type="password" id="inputPassword" class="form-control <?php echo isset($passwordValidation)?$passwordValidation:''; ?>" placeholder="Пароль" required value="<?php echo isset($password)? $password:'';?>">
                                    <label for="inputPassword"><?= _('Password') ?></label>
                                    <?php if (isset($errors['password'])): ?>
                                        <div class="invalid-feedback"><?php echo $errors['password']?></div>
                                    <?php endif; ?>
                                </div>
                                <div class="form-label-group">
                                    <input autocomplete="off" name="repeat-password" type="password" id="inputRepeatPassword" class="form-control <?php echo isset($passwordRepeatValidation)?$passwordRepeatValidation:''; ?>" placeholder="Повторите пароль" required>
                                    <label for="inputRepeatPassword"><?= _('Repeat Password') ?></label>
                                    <?php if (isset($errors['repeat-password'])): ?>
                                        <div class="invalid-feedback"><?php echo $errors['repeat-password']?></div>
                                    <?php endif; ?>
                                </div>
                                <input id="path-files" name="path-files" type="hidden" value="">
                                <button name="signup" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit"><?= _('Sign Up') ?></button>
                            </form>

                            <div class="upload-images">
                                <div class="row ">
                                    <div class="col-md-12 col-sm-12">
                                        <div id="drop-area" class="dm-uploader p-5 text-center">
                                            <p class="mb-3 mt-3 text-muted"><?= _('Drag & drop Files here')?></p>

                                            <div class="btn btn-primary btn-block mb-3">
                                                <span><?= _('Open the file Browser') ?></span>
                                                <input type="file" title="Click to add Files" multiple="">
                                            </div>
                                        </div><!-- /uploader -->

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="card h-100">
                                            <div class="card-header"><?= _('Files')?></div>
                                            <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                                <li class="text-muted text-center empty"><?= _('No files')?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- File item template -->
<script type="text/html" id="files-template">
    <li>
        <div class="media-body mb-1">
            <p class="mb-2">
                <strong>%%filename%%</strong> - <?= _('Status')?>: <span class="text-muted"><?= _('Waiting')?></span>
            </p>
            <div class="progress mb-2">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                     role="progressbar"
                     style="width: 0%"
                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
            <hr class="mt-1 mb-1" />
        </div>
    </li>
</script>
<?php include ROOT. '/views/layouts/footer.php'; ?>