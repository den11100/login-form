<form class="logout form-inline mt-2 mt-md-0" action="/logout" method="post">
    <button class="btn btn-sm btn-outline-secondary" type="submit"><?= _('Exit') ?></button>
</form>
