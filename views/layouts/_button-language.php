<?php if ($_SESSION['lang'] == 'ru'): ?>
    <a class="btn btn-language btn-sm btn-outline-secondary" href="/language/en">EN</a>
<?php else: ?>
    <a class="btn btn-language btn-sm btn-outline-secondary" href="/language/ru">RU</a>
<?php endif; ?>

