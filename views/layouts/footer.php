<?php

use Models\User;

$showFooter = true;
if (isset($action) && $action == "site\index") $showFooter = false;
if (isset($action) && $action == "site\signup") $showFooter = false;

?>

<?php if ($showFooter): ?>
    <div class="clearfix"></div>
    <footer class="footer mt-auto py-3">
        <div class="container">
            <p>Copyright (c) <?php echo date("Y") ?></p>
        </div>
    </footer>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="/template/js/jquery.dm-uploader.min.js"></script>
<script src="/template/js/site.js"></script>


</body>
</html>