<?php

use Models\User;

$hideUpMenu = false;
if (isset($action) && $action == "site\index") $hideUpMenu = true;
if (isset($action) && $action == "site\signup") $hideUpMenu = true;

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <title>Site</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="/template/css/style.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="/template/css/jquery.dm-uploader.min.css" rel="stylesheet" type="text/css" media="screen" />

    </head>
    
<body class="d-flex flex-column h-100">
<?php if ($hideUpMenu): ?>
    <div class="wrap-language">
        <?php include_once "_button-language.php" ?>
    </div>
<?php endif; ?>
<?php if (!User::isGuest()): ?>
    <?php if ($hideUpMenu): ?>
        <div class="wrap-logout">
            <?php include_once "_button-logout.php" ?>
        </div>
    <?php else: ?>
        <nav class="navbar navbar-expand-md navbar-light bg-light up-menu">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
                <?php include_once "_button-language.php" ?>
                <?php include_once "_button-logout.php" ?>
            </div>
        </nav>
    <?php endif; ?>
<?php endif; ?>
