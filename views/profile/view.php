<?php

/* @var array $user */
/* @var array $files */

?>

<?php include ROOT. '/views/layouts/header.php'; ?>

<div class="container">
    <h2><?= _('Profile')?></h2>
    <p>Id: <?php echo $user['id'] ?></p>
    <p>Email: <?php echo $user['email'] ?></p>

    <?php foreach($files as $file) : ?>
        <img src="<?php echo $file ?>" alt="" width="100">
    <?php endforeach;?>
</div>

<?php include ROOT. '/views/layouts/footer.php'; ?>