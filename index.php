<?php

define('ROOT', dirname(__FILE__));
define('LANGUAGES_PATH', ROOT . '/langs');

session_start();

require_once(ROOT.'/vendor/autoload.php');

$params = array_merge(
    require ROOT . '/config/params.php',
    require ROOT . '/config/params_local.php'
);

if ($params['env'] == 'dev') {
    ini_set('display_errors',1);
    error_reporting(E_ALL);
} else {
    ini_set('display_errors',0);
    ini_set('log_errors', 'On');
    ini_set('error_log', ROOT.'/var/log/php_errors.log');
}

if (!isset($_SESSION['lang'])) $_SESSION['lang'] = 'ru';
if ($_SESSION['lang'] == 'ru') {
    $locale = 'ru_RU';
} else {
    $locale = 'en_EN';
}
putenv('LC_ALL=' . $locale);
setlocale(LC_ALL, $locale, $locale . '.utf8');
bind_textdomain_codeset($locale, 'UTF-8');
bindtextdomain($locale, LANGUAGES_PATH);
textdomain($locale);

$router = new Components\Router();
$router->run();